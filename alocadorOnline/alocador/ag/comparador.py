# -*- coding: utf-8 -*-
"""
Created on Fri Nov 11 19:06:37 2016

@author: Filipe Damasceno
"""

from . import DBconector as db

from .Cromossomo import Cromossomo

def comparador(indi1=[],indi2=[],codes=None):
    if codes == None:
        if not type(indi1) is list:
            codes = indi1.cod_papeis
        elif not type(indi2) is list:
            codes = indi2.cod_papeis

    media = 0
    for posi,i in enumerate(indi1):
        
        if i in indi2:
            pos2= indi2.genes.index(i)
            papeli = getPapel(posi,codes)
            papel2 = getPapel(pos2,codes)
            
            
            prod1,exp1,cust1 = db.Get.Papeis(i,papeli)
            prod2,exp2,cust2 = db.Get.Papeis(i,papel2)
            
            cust1 = (cust1-30)/170
            cust2 = (cust2-30)/170
            
            afini1,afini2 = afinidades(i,indi1),afinidades(i,indi2)
            prod = abs((prod1-prod2)/100)
            exp = abs((exp1-exp2)/100)
            cust = abs(cust1-cust2)
            afini = abs((afini1-afini2)/10)
            
            media += 1-sum([prod,exp,cust,afini])/4
    
    
    media/=len(indi1)
    return media

def getPapel(pos,decode):
    for j,i in enumerate(decode):
        if pos in i:
            return j

def afinidades(ind,todos):
    val=0
    afin=dict(zip(["Ruim","regular","Bom","Excelente"],[1,4,7,10]))
    for i in todos:
        if i != ind:
            val+= afin[db.Get.Afinidade(ind,i)]#TODO: modificar para string
    return val/(len(todos)-1)

def afinidade(todos):
    val = 0
    for i in todos:
        val+= afinidades(i,todos)
    return val/len(todos)

def avaliarDados(lista,base):
    out = []
    for pos,i in enumerate(lista):
        r= comparador(i, base)
        out.append(int(100*r))
    return out

def avaliarMassa(lista,base,caminho):
    out = []
    for pos,i in enumerate(lista):
        r= comparador(i, base)
        out.append(Resultado(i,base,int(100*r),caminho[pos]))
    return out

class Resultado(object):
    def __init__(self,comparado,base,resultado,caminho):
        self._comparado = comparado
        self._base = base
        self._resultado = resultado
        self.caminho=caminho
    
    @property
    def comparado(self):
        return self._comparado
    @property
    def base(self):
        return self._base
    @property
    def resultado(self):
        return self._resultado
    @property
    def grafico(self):
        return self.caminho
    
        