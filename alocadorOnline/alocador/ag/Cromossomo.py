'''
Created on 12 de mar de 2016

@author: Filipe Damasceno
'''
from copy import deepcopy as cpy
import random
import numpy as np
from Cython.Shadow import returns


class Cromossomo(object):
    '''
    esta classe contem um conjunto de genes
    '''

    def __init__(self, tamanho=0, ids=[],codificacao=[],DB=None,genes=None,decode=None):
        '''
        gera um cromossomo contendo os genes es funcoes atribuidas a ela.
        codificacao possui uma lista para definir a codificacao
        '''
        self.recurso = DB
        self.__codificado = codificacao
        self.__cod_papeis = None if decode == None else decode
        if self.__cod_papeis == None:
            self.__decodificador(codificacao)
        self.__ids = ids
        self.__genes = random.sample(self.__ids,tamanho) if genes == None else genes
        self.__equipe=-1
        '''
        :paran pos 0 :=> afinidade
        :paran pos 1 :=> salario
        :paran pos 2 :=> produtividade
        :paran pos 3 :=> experiencia
        :paran pos 4 :=> especialidades //not implemented
        :paran pos 5 :=> tecnologias // not implemented
        '''
        self.__objetivos = [0,0,0,0]
        self.rank = float('inf')
        self.diatancia = 0 
        self.dominados = set()
        self.contador_dominado = 0
        if genes:
            self.calcfitness()

    
    def __decodificador(self,codificacao):
        '''
        recebe o numero de agentes em cada papel no formato: [1,2,3,4]
        onde:
        1 -> N analistas 
        2 -> N arquitetos
        3 -> N programadores
        4 -> N testadores
        realiza uma soma acumulada destes valores no vetor, para o formato: [1,3,6,10]
        separando apos isso os ids de cada passoa separando os vetores de papeis, para o formato
        [[0,1],[1,2,3],[3,4,5,6],[6,7,8,9,10]], ou seja dependendo da posicao do agente
        ele exerce um papel diferente no projeto.
        '''
        cod = cpy(codificacao)
        for i in range(1,len(codificacao)):
            cod[i]+=cod[i-1]
        vtac = []
        for i in range(len(cod)):
            if i==0:
                vtac.append(range(0,cod[i]))
            else:
                vtac.append(range(cod[i-1],cod[i]))
        self.__cod_papeis = vtac
    '''
    +=+=+=+=+=+=+=+=+=+=+=+=+get,set+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
    '''
    @property
    def equipe(self):
        return self.__equipe
    
    @equipe.setter
    def equipe(self,val):
        self.__equipe=val
    
    @property
    def genes(self):
        return cpy(self.__genes)
    @genes.setter
    def genes(self, genes):
        if genes.__class__ == list and genes[0].__class__ == int:
            self.__genes = cpy(genes)
    @genes.deleter
    def genes(self):
        del self.__genes

    @property
    def objetivos(self):
        return self.__objetivos
    
    @property
    def cod_papeis(self):
        return self.__cod_papeis

    '''
    +=+=+=+=+=+=+=+=+=+=+=+=+func fitness+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
    '''
    def calcfitness(self):
        #TODO: fazer uma funcao no dbconector pra acelerar esse processo. 
        papeis,afinidade = self.recurso.getFitness(self.genes,self.__cod_papeis)
        afinidades=dict(zip(["Ruim","regular","Bom","Excelente"],[1,4,7,10]))
        
        afinidade = sum([afinidades[i[0][0]] for i in afinidade])/len(afinidade) if len(afinidade) != 0 else 0
        
        papeis = np.array(papeis).T
        
        prod = sum(papeis[0][0])/len(self)
        expe = sum(papeis[1][0])/len(self)
        valor = sum(papeis[2][0])/len(self)    
        
        '''
        afinidade = (sum([afinidades[i[0][0]] for i in afinidade])/len(afinidade) if len(afinidade) != 0 else 0)/10
        
        papeis = np.array(papeis).T
        
        prod = (sum(papeis[0][0])/len(self))/100
        expe = (sum(papeis[1][0])/len(self))/100
        valor = ((sum(papeis[2][0])/len(self))-30)/170 
            
       #'''
        
        '''
        self.__fitnessafinidade(recursos)
        self.__fitnessExperiencia(recursos)
        self.__fitnessPreco(recursos)
        self.__fitnessProdutividade(recursos)
        #'''
       
        self.__objetivos = [afinidade, valor, prod, expe]
        
        
    def __fitnessafinidade(self):
        acc=0 
        for i in self:
            for j in self:
                if i != j:
                    acc += self.recurso.getAfinidades(i,j)
        self.__objetivos[0] = 1/(acc/(len(self)*(len(self)-1)))
    
    def __fitnessPreco(self):
        media_precos=0
        for i in range(len(self)):    
            media_precos += self.recurso.getPreco(self[i],self.getPapel(i))#agente.buscarPapel(self.__getPapel(i)).preco
        media_precos/= len(self)
        self.__objetivos[1] = media_precos
    
    def __fitnessExperiencia(self):        
        acc=0
        for i in range(len(self)):   
            acc += self.recurso.getExperiencia(self[i], self.getPapel(i))
        self.__objetivos[3] = 1/(acc/len(self))
    
    def __fitnessProdutividade(self):
        acc=0
        for i in range(len(self)):    
            acc += self.recurso.getProducao(self[i],self.getPapel(i))
        self.__objetivos[2] = 1/(acc/len(self))
        
    def getPapel(self,pos=0):
        for i in range(len(self.__cod_papeis)):
            if pos in self.__cod_papeis[i]:
                return i
    
    def mutacao(self,taxa=0.05):  
        self.__ids     
        for i in range(len(self)):
            if random.random() < taxa:
                self[i] = self.__new_gene(self.genes, self[i])
    
    def __new_gene(self,genes,individuo_atual=0):
        import random as rand
        novo = self.__ids.index(individuo_atual)
        novov = individuo_atual
        while novov in genes or not novov in self.__ids:
            randVal = rand.random()
            moeda = 1 if rand.random() > 0.5 else 0 #1 = cara casasAcima, 0 = coroa casasAbaixo
            if randVal <= 0.5:
                novo+= 1 if moeda else -1
            elif randVal <= 0.75:
                novo+= 2 if moeda else -2
            elif randVal <= 0.9:
                novo+= 3 if moeda else -3
            else:
                novo= random.choice(range(len(self.__ids)))
            
            if novo >= len(self.__ids):
                novo = len(self.__ids) - novo
            elif novo < 0:
                novo = -1 *novo
            novov = self.__ids[novo]
            #print("ta aqui",genes,individuo_atual,novo,novov,self.__ids)
        return self.__ids[novo]
    
    def domina(self,outro=None):
        '''
        verifica se esta solucao domina a outra.
        :param outro: outro individuo
        '''
        '''for i in range(len(self.objetivos)):
            if self.objetivos[i] > outro.objetivos[i]:
                return False''' # Provavelmente inutil!
        for i in range(len(self.objetivos)):
            if self.objetivos[i] < outro.objetivos[i]:
                return True
        return False
    '''
    +=+=+=+=+=+=+=+=+=+=+=+=+sobrecarga+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
    '''
    @property
    def codificador(self):
        return [self.__cod_papeis,self.genes]
    @property
    def analistas(self):
        resultado = "Analista de requisitos: "
        pos = 0
        for i in self.__cod_papeis[pos]:
            resultado+= self.recurso.getNomeAgentes(self[i])+", "
        return resultado
    @property
    def arquitetos(self):
        pos = 1
        resultado = "Arquiteto(a) de software: "
        for i in self.__cod_papeis[pos]:
            resultado+= self.recurso.getNomeAgentes(self[i])+", "
        return resultado
    @property
    def programadores(self):
        pos = 2
        resultado = "Programador(a): "
        for i in self.__cod_papeis[pos]:
            resultado+= self.recurso.getNomeAgentes(self[i])+", "
        return resultado
    @property
    def testadores(self):
        pos = 3
        resultado = "Testador(a): "
        for i in self.__cod_papeis[pos]:
            resultado+= self.recurso.getNomeAgentes(self[i])+", "
        return resultado
    
    @property
    def afinidade(self):
        return self.objetivos[0]
    @property
    def custo(self):
        return self.objetivos[1]
    @property
    def experiencia(self):
        return self.objetivos[3]
    @property
    def produtividade(self):
        return self.objetivos[2]
    
    @property
    def detalhes(self):
        todos=[]
        nomes = ["Analista de requisitos","Arquiteto(a) de software","Programador(a)","Testador(a)"]
        for pos,i in enumerate(self.genes):
            agente = []
            agente.append(i)
            agente.append(self.recurso.getNomeAgentes(i))
            p = self.getPapel(pos)
            agente.append(nomes[p])
            agente.append(self.recurso.getPreco(i,p))
            agente.append(self.recurso.getExperiencia(i, p))
            agente.append(self.recurso.getProducao(i,p))
            afinidade =[]
            for j in self.genes:
                if i != j:
                    afinidade.append(self.recurso.getNomeAgentes(j)+": "+self.recurso.getAfinidades(i,j))
            agente.append(afinidade)
            todos.append(agente)
            
        return todos
            
            
    
    def decode(self):
        nomes = ["Analista de requisitos","Arquiteto(a) de software","Programador(a)","Testador(a)"]
        codificacao = ""
        for cdPapel,agentes in enumerate(self.__cod_papeis):
            codificacao += nomes[cdPapel]+": "
            for pos in agentes:
                codificacao += self.recurso.getNomeAgentes(self[pos])+", "
            codificacao += "\n"
        return codificacao
            
    def copy(self):
        copia = self.__class__(0,self.__ids,self.__codificado,self.recurso)
        copia.genes = cpy(self.genes)
        return copia
        
    def __getitem__(self,index=-1): # Cromossomo[index]
        return self.__genes[index]
    
    def __setitem__(self,index,valor): # Cromossomo[index]
        if valor in self.__ids:
            self.__genes[index] = valor
    
    def __len__(self):
        return len(self.__genes)
    
    def __copyset(self,pai,posicao,filho):
            acc =[]
            if len(filho) < len(self):
                for i in pai[posicao:]+pai[:posicao]:
                    if not (i in filho):
                        acc.append(i)
                        if len(acc+filho) >= len(self):
                            break
            return acc
    
    def __add__(self,direita):#crossOver 
        '''
        realiza CrossOver, de modo a facilitar o processo 
        foi sobrecarregado o somador.        
        '''
        posicao = random.randint(0,self.__len__())
                
        filho1 = self[:posicao]
        filho2 = direita[:posicao]
        filho1+= self.__copyset(direita, posicao, filho1)
        filho2+= self.__copyset(self, posicao, filho2)
        f1 = Cromossomo(len(self), self.__ids, self.__codificado,self.recurso)
        f2 = Cromossomo(len(self), self.__ids, self.__codificado,self.recurso)
        f1.genes = filho1
        f2.genes = filho2
        del filho1,filho2
        
        return [f1,f2] 
    
    def __str__(self):
        
        return self.decode()+'''><><><><><><><><><><><><><><><><><><><><><><>
Cromossomos: {0}
--------------------Fitness------------------
Afinidade:---------------{1}
Custo:-------------------{2}
Produtividade:-----------{3}
Experiencia:-------------{4}
'''.format(str(self.genes),\
                   str(self.__objetivos[0]),\
                   str(self.__objetivos[1]),\
                   str(self.__objetivos[2]),\
                   str(self.__objetivos[3]))
    @property
    def result(self):
        return self.__str__()