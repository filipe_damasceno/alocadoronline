import random as r
from .DBconector import DB as db
import os
from unicodedata import normalize
varia = ""

def geraAfinidade(nome,lista):
    '''lista no formato : [[nome,afinidade],[]...]'''
    val =nome+"\\nnome: afinidade\\n"
    
    for i,afinidade in lista:
        val+= normalize('NFKD',str(i)).encode('ASCII', 'ignore').decode()+": "+str(afinidade)+"\\n"
    
    return '<td>' \
           '<div class="container-contact100-form-btn">' \
					'<button class="contact100-form-btn" id="btn" onclick="afinidade('+"'"+ val +"'"+');">'\
    					'<span>'\
							'Afinidade'\
							'<i class="fa fa-long-arrow-up m-l-7" aria-hidden="true"></i>'\
						'</span>'\
					'</button>'\
				'</div>' \
            '</td>'

def geraProdutividade(nome,prod):
    val =nome+"\\npapel  <==>  valor\\n"
    papeis = ["analista de requisitos","arquiteto de software","programador","testadores"]
    for i,j in zip(prod,papeis):
        val += j+"  ==>  "+str(i)+"\\n"
    return '<td>' \
           '<div class="container-contact100-form-btn">' \
					'<button class="contact100-form-btn" id="btn" onclick="produtividade('+"'"+val+"'"+');">'\
    					'<span>'\
							'Produtividade'\
							'<i class="fa fa-long-arrow-up m-l-7" aria-hidden="true"></i>'\
						'</span>'\
					'</button>'\
				'</div>' \
            '</td>'

def geraExperiencia(nome,experiencias):
    val =nome+"\\npapel  <==>  valor\\n"
    papeis = ["analista de requisitos","arquiteto de software","programador","testador"]
    for i,j in zip(experiencias,papeis):
        val += j+"  ==>  "+str(i)+"\\n"
    return '<td>' \
           '<div class="container-contact100-form-btn">' \
					'<button class="contact100-form-btn" id="btn" onclick="expereincia('+"'"+val+"'"+');">'\
    					'<span>'\
							'Experiencia'\
							'<i class="fa fa-long-arrow-up m-l-7" aria-hidden="true"></i>'\
						'</span>'\
					'</button>'\
				'</div>' \
            '</td>'

def geraCusto(nome,custos):
    val =nome+"\\npapel  <==>  valor\\n"
    papeis = ["analista de requisitos","arquiteto de software","programador","testadores"]
    for i,j in zip(custos,papeis):
        val += j+"  ==>  "+str(i)+"\\n"
    return '<td>' \
                '<div class="container-contact100-form-btn">' \
					'<button class="contact100-form-btn" id="btn" onclick="Custo('+"'"+val+"'"+');">'\
    					'<span>'\
							'Custo'\
							'<i class="fa fa-long-arrow-up m-l-7" aria-hidden="true"></i>'\
						'</span>'\
					'</button>'\
				'</div>' \
            '</td>'

def geraAgente(nome,codigo,afinidades,produtividade,experiencia,custo):
    #
    nome = normalize('NFKD',nome).encode('ASCII', 'ignore').decode()
    return "<tr><td>"+str(codigo)+\
        "</td><td>"+nome+\
        "</td> "+str(geraAfinidade(nome,afinidades))+\
        str(geraProdutividade(nome,produtividade))+\
        str(geraExperiencia(nome,experiencia))+\
        str(geraCusto(nome,custo))+"</tr>\n"

def gerarPessoas(quantidade = 20):
    '''funcao que vai fazer as requisiçoes mo BD'''
    dados = ""
    ids = db.getIdsAgente()
    escolidos = r.sample(ids,quantidade)
    for i in escolidos:
        afinidade = []
        produtividade= []
        experiencia = []
        custo = []
        for k in range(4):
            produtividade.append(db.getProducao(i,k))
            experiencia.append(db.getExperiencia(i,k))
            custo.append(db.getPreco(i,k))
        for j in escolidos:
            if i != j:
                afinidade.append([db.getNomeAgentes(j),db.getAfinidades(i,j)])
        dados+= geraAgente(db.getNomeAgentes(i),i,afinidade,produtividade,experiencia,custo)
        
    
    return dados,escolidos


def gerarPagina(n=20):
    dados,escolidos = gerarPessoas(n)
    val = '''
    {% load static %}
    <!DOCTYPE html>
    <html>
        <title>Alocacao Avaliacao</title>
        
        <head>
            <meta charset="utf-8">
            <script type="text/javascript" src='{% static "JavaScript/para_formulario.js" %}'></script>
            <title>Contact V5</title>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1" %}'>
        <!--===============================================================================================-->
            <link rel="icon" type="image/png" href=' {% static "images/icons/favicon.ico" %}'/>
        <!--===============================================================================================-->
            <link rel="stylesheet" type="text/css" href=' {% static "vendor/bootstrap/css/bootstrap.min.css" %}'>
        <!--===============================================================================================-->
            <link rel="stylesheet" type="text/css" href=' {% static "fonts/font-awesome-4.7.0/css/font-awesome.min.css" %}'>
        <!--===============================================================================================-->
            <link rel="stylesheet" type="text/css" href=' {% static "fonts/iconic/css/material-design-iconic-font.min.css" %}'>
        <!--===============================================================================================-->
            <link rel="stylesheet" type="text/css" href=' {% static "vendor/animate/animate.css" %}'>
        <!--===============================================================================================-->
            <link rel="stylesheet" type="text/css" href=' {% static "vendor/css-hamburgers/hamburgers.min.css" %}'>
        <!--===============================================================================================-->
            <link rel="stylesheet" type="text/css" href=' {% static "vendor/animsition/css/animsition.min.css" %}'>
        <!--===============================================================================================-->
            <link rel="stylesheet" type="text/css" href=' {% static "vendor/select2/select2.min.css" %}'>
        <!--===============================================================================================-->
            <link rel="stylesheet" type="text/css" href=' {% static "vendor/daterangepicker/daterangepicker.css" %}'>
        <!--===============================================================================================-->
            <link rel="stylesheet" type="text/css" href=' {% static "vendor/noui/nouislider.min.css" %}'>
        <!--===============================================================================================-->
            <link rel="stylesheet" type="text/css" href=' {% static "css/util.css" %}'>
            <link rel="stylesheet" type="text/css" href=' {% static "css/main.css" %}'>
        <!--===============================================================================================-->
        </head>
        
        <body>
        <div class="container-contact100">
            <div class="wrap-contact100">
                <a class="label-input100" href="{% url 'alocador:menu' id %}">Menu</a>
                <a class="label-input100" href="{% url 'alocador:logout' id %}">Sair</a>
                <span class="contact100-form-title">
                    ALOCAR EQUIPE.
                </span>        
                <form action="{% url 'alocador:salvarDados' id %}" method="post">
                    {% csrf_token %}
                     <span class="label-input100">
                        {{analistas}} analista(s), {{arquitetos}} arquiteto(s), {{programadores}} programador(es), {{testadores}} testador(es).
                    </span>
                    
                    <div class="wrap-input100 validate-input bg1" data-validate="DEVE ENTRAR COM A EQUIPE ALOCADO: 10,11,12..,13 !">
                        <span class="label-input100">EQUIPE ALOCADA</span>
                        <input class="input100" name=alocacao id=alocacao type=text placeholder="DEVE ENTRAR COM A EQUIPE ALOCADO: 10,11,12..,13 !">
                    </div>
                    <input name=Nids id=Nids value="{{ Nids }}" type="hidden">
                    <input name=analistas id=analistas value="{{ analistas}}" type="hidden">
                    <input name=arquitetos id=arquitetos value="{{ arquitetos}}" type="hidden">
                    <input name=programadores id=programadores value="{{ programadores}}" type="hidden">
                    <input name=testadores id=testadores value="{{ testadores}}" type="hidden">
                    <input name=IDs value="'''+str(escolidos)[1:-1]+'''" type="hidden">
                    
                    <input name=afinidade id=afinidade value=0 type="hidden">
                    <input name=produtividade id=produtividade value=0 type="hidden">
                    <input name=experiencia id=experiencia value=0 type="hidden">
                    <input name=custo id=custo value=0 type="hidden">
                    
                    <input name=segundos id=segundos value=0 type="hidden">
                    <input name=minutos id=minutos value=0 type="hidden">
                    <input name=horas id=horas value=0 type="hidden">
                    
                    <td>
                    <div class="container-contact100-form-btn">
                        <button class="contact100-form-btn" id="btn" onclick="Custo('+"'"+val+"'"+');">
                            <span>
                                CADASTRAR
                                <i class="fa fa-long-arrow-down m-l-7" aria-hidden="true"></i>
                            </span>
                        </button>
                    </div>
                    </td>
                </form> 
                <table>
                    <tr>
                        <th>[codigo]</th>
                        <th>nome</th> 
                        <th>Afinidade</th> 
                        <th>Produtividade</th> 
                        <th>Experiencia</th> 
                        <th>custo</th>
                    </tr>
                    '''+ dados +'''
                </table>
            </div>
        </div>
            <!--===============================================================================================-->
                <script src=' {% static "vendor/jquery/jquery-3.2.1.min.js" %} '></script>
            <!--===============================================================================================-->
                <script src=' {% static "vendor/animsition/js/animsition.min.js" %} '></script>
            <!--===============================================================================================-->
                <script src=' {% static "vendor/bootstrap/js/popper.js" %} '></script>
                <script src=' {% static "vendor/bootstrap/js/bootstrap.min.js" %} '></script>
            <!--===============================================================================================-->
                <script src=' {% static "vendor/select2/select2.min.js" %} '></script>
                <script>
                    $(".js-select2").each(function(){
                        $(this).select2({
                            minimumResultsForSearch: 20,
                            dropdownParent: $(this).next('.dropDownSelect2')
                        });
            
            
                        $(".js-select2").each(function(){
                            $(this).on('select2:close', function (e){
                                if($(this).val() == "Please chooses") {
                                    $('.js-show-service').slideUp();
                                }
                                else {
                                    $('.js-show-service').slideUp();
                                    $('.js-show-service').slideDown();
                                }
                            });
                        });
                    })
                </script>
            <!--===============================================================================================-->
                <script src=' {% static "vendor/daterangepicker/moment.min.js" %} '></script>
                <script src=' {% static "vendor/daterangepicker/daterangepicker.js" %} '></script>
            <!--===============================================================================================-->
                <script src=' {% static "vendor/countdowntime/countdowntime.js" %} '></script>
            <!--===============================================================================================-->
                <script src=' {% static "vendor/noui/nouislider.min.js" %} '></script>
                <script>
                    var filterBar = document.getElementById('filter-bar');
            
                    noUiSlider.create(filterBar, {
                        start: [ 1500, 3900 ],
                        connect: true,
                        range: {
                            'min': 1500,
                            'max': 7500
                        }
                    });
            
                    var skipValues = [
                    document.getElementById('value-lower'),
                    document.getElementById('value-upper')
                    ];
            
                    filterBar.noUiSlider.on('update', function( values, handle ) {
                        skipValues[handle].innerHTML = Math.round(values[handle]);
                        $('.contact100-form-range-value input[name="from-value"]').val($('#value-lower').html());
                        $('.contact100-form-range-value input[name="to-value"]').val($('#value-upper').html());
                    });
                </script>
            <!--===============================================================================================-->
                <script src=' {% static "js/main.js" %} '></script>
            
            <!-- Global site tag (gtag.js) - Google Analytics -->
            <script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
            <script>
              window.dataLayer = window.dataLayer || [];
              function gtag(){dataLayer.push(arguments);}
              gtag('js', new Date());
            
              gtag('config', 'UA-23581568-13');
            </script>
        </body>
    </html>'''
    BASE_DIR = os.path.split(os.path.dirname(os.path.abspath(__file__)))[0]
    filepath = os.path.join(BASE_DIR,"templates\\testes\\pagina.html")
    arq = open(filepath,"w")
    arq.write(val)
    arq.close()