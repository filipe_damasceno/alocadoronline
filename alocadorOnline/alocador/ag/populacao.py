'''
Created on 12 de mar de 2016

@author: Filipe Damasceno
'''


class Populacao(object):
    '''
    nessa classe estarao um conjunto de individuos
    '''


    def __init__(self,cromossomos=[]):
        '''
        havera um numero grande de individuos, de sorte que tem que adicionar muitos individuos.
        '''
        self.__cromossomos = cromossomos
        
        self.frentes = []
        
        self.__melhoresFrentes = []
    
    @property
    def cromossomos(self):
        return self.__cromossomos
    @cromossomos.setter
    def cromossomos(self,c):
        if type(c) == type([]):
            self.__cromossomos = c
    @cromossomos.deleter
    def cromossomos(self):
        del self.__cromossomos
        
    def getDadosEstatisticos(self):
        return self.__melhoresFrentes
    
    def salvarfrente(self):                    
        self.__melhoresFrentes = [self.frentes[0][i] for i in range(len(self))] if len(self.frentes[0])> len(self) else self.frentes[0]
    
    def retorna_frente(self,recursos):
        frente = []
        frente.append(max(self.frentes[0],key = lambda objetivo: objetivo.objetivos[0]).objetivos)
        frente.append(min(self.frentes[0],key = lambda objetivo: objetivo.objetivos[1]).objetivos)
        frente.append(max(self.frentes[0],key = lambda objetivo: objetivo.objetivos[2]).objetivos)
        frente.append(max(self.frentes[0],key = lambda objetivo: objetivo.objetivos[3]).objetivos)
        val="++++++++++++++++++++++++++ dados medianos +++++++++++++++++++++++++++++++++++\n"+str(len(self.__melhoresFrentes))+"\n"
        for i in self.__melhoresFrentes:
            if not(i.objetivos in frente):
                frente.append(i.objetivos)
                val+=i.decode()+"\n\n"
        return val
    
    def retorna_melhores(self,):
        return'''
++++++++++++++ melhores equipes +++++++++++++

melhor em custo:

{0}

melhor em produtividade:

{2}

melhor em experiencia:

{3}

melhor em afinidade:

{1}

'''.format(min(self.frentes[0],key = lambda custo: custo.objetivos[1]).decode(),\
           max(self.frentes[0],key = lambda afinidade: afinidade.objetivos[0]).decode(),\
           max(self.frentes[0],key = lambda produtividade: produtividade.objetivos[2]).decode(),\
           max(self.frentes[0],key = lambda experiencia: experiencia.objetivos[3]).decode())

    def retornarMelhores(self):
        return [min(self.frentes[0],key = lambda custo: custo.objetivos[1]),\
           max(self.frentes[0],key = lambda afinidade: afinidade.objetivos[0]),\
           max(self.frentes[0],key = lambda produtividade: produtividade.objetivos[2]),\
           max(self.frentes[0],key = lambda experiencia: experiencia.objetivos[3])]
    """
    -----------sobrecargas----------------
    """
    
    def __len__(self):
        return len(self.__cromossomos)
    
    def __getitem__(self, index):
        return self.__cromossomos[index]
    
    def __setitem__(self, index,valor):
        if type(valor) == type(self[0]):
            self.__cromossomos[index] = valor
    def __str__(self, *args, **kwargs):
        v=""
        for i in self:
            v+= str(i)+"\n"+"+++"+str(len(i))+"+++\n"
        return v