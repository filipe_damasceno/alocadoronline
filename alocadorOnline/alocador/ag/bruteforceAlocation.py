from DBconector import DB
from Cromossomo import Cromossomo
from itertools import permutations

from genetcAlgoritm import AG

def gerarIndividuo(ids, codificacao, individuo):
    return Cromossomo(tamanho=len(individuo),ids=ids,DB=DB,genes=list(individuo),decode=codificacao)

def central(indis):
    diciona = dict(zip(indis,[0 for _ in range(len(indis))]))
    melhores = []
    for i in range(4):
        if i != 1:
            indis.sort(key= lambda v: v.objetivos[i], reverse=True)
        else:
            indis.sort(key= lambda v: v.objetivos[i])
        melhores.append(indis[0])
        for pos,ind in enumerate(indis):
            diciona[ind]+=pos
        result = [(i,diciona[i]) for i in list(diciona)]
        result.sort(key=lambda v: v[1])
    return [result[0][0]]+melhores

def bruteForce(ids,cod):
    individuos = []
    tamanhoCromo = sum([(1+i[-1])-i[0] for i in cod])
    for ind in permutations(ids,tamanhoCromo):
        individuos.append(gerarIndividuo(ids,cod,ind))
    return central(individuos)

def radar(ag, bruteforce):
    
    import numpy as np
    import pylab as pl
    
    class Radar(object):
    
        def __init__(self, fig, titles, labels, rect=None):
            if rect is None:
                rect = [0.05, 0.05, 0.95, 0.95]
    
            self.n = len(titles)
            self.angles = np.arange(90, 90+360, 360.0/self.n)
            self.axes = [fig.add_axes(rect, projection="polar", label="axes%d" % i) 
                             for i in range(self.n)]
    
            self.ax = self.axes[0]
            self.ax.set_thetagrids(self.angles, labels=titles, fontsize=30)
    
            for ax in self.axes[1:]:
                ax.patch.set_visible(False)
                ax.grid("on")
                ax.xaxis.set_visible(False)
    
            for ax, angle, label in zip(self.axes, self.angles, labels):
                ax.set_rgrids(range(1, 11), angle=angle, labels=label)
                ax.spines["polar"].set_visible(True)#False)
                ax.set_ylim(0, 5)
    
        def plot(self, values, *args, **kw):
            angle = np.deg2rad(np.r_[self.angles, self.angles[0]])
            values = np.r_[values, values[0]]
            self.ax.plot(angle, values, *args, **kw)
    
    
    cores = ["r","b","g","y","c"]
    nomes = ["custo_beneficio","Custo","Afinidade","Produtividade","Experiencia"]
    cont=1
    def normalize(v):
        return [(5*int(v[0]))/10,5*(1-((float(v[1])-30)/(200-30))),(5*int(v[2]))/99,(5*int(v[3]))/99]
    
    for nome,cor,dados in zip(nomes,cores,zip(fitness,bruteforce)):
    
        fig = pl.figure(figsize=(9, 8))
        
        titles = ["afinidade","custo","produtividade","experiencia"]#list("ABCDE")
        
        labels = [[""]*5]*4#[["20","40","60","80","100"] for i in range(4)]
        radar = Radar(fig, titles, labels,[0.1, 0.1, 0.80, 0.80])
    #     cores = ["k","r","b","g","y","c"]
    #     nomes = ["sua Alocacao","custo_beneficio","Custo","Afinidade","Produtividade","Experiencia"]
    #     radar = Radar(fig, titles, labels,[0.1, 0.1, 0.80, 0.80])
    #     for nome,cor,dados in zip(nomes,cores,fitness):
            #print(nome,cor,dados,normalize(dados))
        
        radar.plot(normalize(dados[0]),  "-p", lw=2, color="k", alpha=1, label="sua Alocacao")
        radar.plot(normalize(dados[1]),  "-p", lw=2, color=cor, alpha=1, label=nome)
        #radar.plot(normalize([10,200,99,99]),"-p", lw=2, color="b", alpha=1, label="second")
        #radar.plot([3, 4, 4, 2.3], "-p", lw=2, color="g", alpha=0.4, label="third")
        radar.ax.legend()
        BASE_DIR = os.path.split(os.path.dirname(os.path.abspath(__file__)))[0]
        filepath = os.path.join(BASE_DIR,"alocador\\static\\graficos\\"+str(cont)+".JPEG")
        cont+=1
        fig.savefig(filepath)
        del fig
    del radar,dados
    #enter image description here

