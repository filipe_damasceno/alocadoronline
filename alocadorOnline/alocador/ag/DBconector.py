'''
Created on 27 de mar de 2016

@author: Filipe Damasceno
'''
bancoName = 'recursosteste.db'
import os.path

import sqlite3 as sql

db = None

class DB():
    
    @staticmethod
    def fechar():
        global db
        db.close()
    @staticmethod
    def abrir(): 
        global db
        #db = None    
        BASE_DIR = os.path.dirname(os.path.abspath(__file__))
        db_path = os.path.join(BASE_DIR, bancoName)
        db = sql.connect(db_path)
        
    
    @staticmethod
    def criar_tables():
        con = sql.connect(bancoName)
        cursor = con.cursor()
        '''tabela de agentes'''
        #'''
        cursor.execute(""" 
            CREATE TABLE Agentes (
                id INTEGER NOT NULL PRIMARY KEY,
                nome TEXT NOT NULL
            );
        """) #adicionar acarga horaria disponivel (diaria), verificação se este esta ocupado (viajando, mudou-se, dixou a empresa bla bla bla bla.) 
        
        #tabela de papeis
        
        cursor.execute(""" 
            CREATE TABLE Papeis (
                idpapel INTEGER NOT NULL,
                idAgente INTEGER NOT NULL,
                nome TEXT NOT NULL,
                producao INTEGER NOT NULL,
                experiencia INTEGER NOT NULL,
                valor REAL NOT NULL,
                FOREIGN KEY (idAgente) REFERENCES Agentes(id),
                PRIMARY KEY (idpapel, idAgente)
            );
        """)
        
        cursor.execute(""" 
            CREATE TABLE Afinidades (
                idAgente INTEGER NOT NULL,
                idOutro INTEGER NOT NULL,
                afinidade TEXT NOT NULL,
                FOREIGN KEY (idAgente) REFERENCES Agentes(id),
                FOREIGN KEY (idOutro) REFERENCES Agentes(id),
                PRIMARY KEY (idOutro, idAgente)
            );
        """)
        
        '''
        cursor.execute("""
            CREATE TABLE periodoDeFerias (
                idferias INTEGER UNSIGNED NOT NULL,
                idAgente INTEGER UNSIGNED NOT NULL,
                dataInicio DATE NULL,
                dataFim DATE NULL,
                PRIMARY KEY(idferias),
                FOREIGN KEY (idAgente) REFERENCES Agentes(id)
            );
        """)
        #'''
        con.close()
        print("tabelas criadas com sucesso!!!!!!")
    
    @staticmethod
    def add_Agente(id,nome):
        Add.Agente(id, nome)
    
    @staticmethod
    def add_Papel(idagente,idpapel,nome,produtividade,experiencia,valor):
        Add.Papel(idagente, idpapel, nome, produtividade, experiencia, valor)
    
    @staticmethod      
    def add_Afinidade(ag1,ag2,afinidade):
        Add.Afinidade(ag1, ag2, afinidade)
   
    @staticmethod
    def getAfinidades(agente,outro):
        return Get.Afinidade(agente, outro)
    
    @staticmethod       
    def getPapeis(idagente,idpapel):
        return Get.Papeis(idagente, idpapel)
        
    @staticmethod
    def getPreco(idagente,idpapel):
        return Get.Preco(idagente, idpapel)
    
    @staticmethod
    def getExperiencia(idagente,idpapel):
        return Get.Experiencia(idagente, idpapel)
    
    @staticmethod
    def getProducao(idagente,idpapel):
        return Get.Producao(idagente, idpapel)
    
    @staticmethod
    def getIdsAgente():
        return Get.IdsAgente()
    
    @staticmethod
    def getAgentes():
        return Get.Agentes()
    
    @staticmethod
    def getNomeAgentes(ida):
        return Get.NomeAgentes(ida)
    
    @staticmethod
    def getFitness(cromossomo,papeis):
        return Get.fitness(cromossomo, papeis)
    
    @staticmethod
    def addRandDate():
        Dados_rand.add()
        
class Add:
    
    @staticmethod
    def Agente(id,nome):
        global db
        DB.abrir()
        
        cursor = db.cursor()
        cursor.execute("""
        INSERT INTO Agentes (id, nome) VALUES (?,?)
        """,(id, nome))
        db.commit()
        
    @staticmethod
    def Papel(idagente,idpapel,nome,produtividade,experiencia,valor):
        global db
        DB.abrir()
        
        cursor = db.cursor() 
        cursor.execute("""
        INSERT INTO Papeis (idpapel, idAgente, nome, producao, experiencia, valor) VALUES (?,?,?,?,?,?)
        """,(idpapel, idagente, nome, produtividade, experiencia, valor))
        db.commit()
    
    @staticmethod      
    def Afinidade(ag1,ag2,afinidade):
        global db
        DB.abrir()
        
        cursor = db.cursor() 
        cursor.execute("""
        INSERT INTO Afinidades (idAgente, idOutro, afinidade) VALUES (?,?,?)
        """,(ag1,ag2,afinidade))
        db.commit()
    
    @staticmethod
    def ferias(agente,idferias,datainit,datafim):#TODO: vai cupadi
        global db
        DB.abrir()
        
        cursor = db.cursor() 
    

class Get:
    
    @staticmethod
    def IdWithDate(init,fim):
        import time
        initD = time.strptime(init, "%d/%m/%Y")
        fimD = time.strptime(fim, "%d/%m/%Y")
        
        global db
        DB.abrir()
        cursor = db.cursor()
        ids = Get.IdsAgente()
        idsResult = []
        for i in ids:
            cursor.execute("""
            SELECT dataInicio, dataFim FROM periodoDeFerias WHERE idAgente = ?;
        """,(i,))
            
            datas= cursor.fetchall()
            aceita = True
            for j in datas:
                di = time.strptime(j[0], "%d/%m/%Y")
                df = time.strptime(j[1], "%d/%m/%Y")
                
                aceita = aceita and ((di < initD and df <=initD) or (di >= fimD))
            if aceita:
                idsResult.append(i)
        return idsResult
    
    @staticmethod
    def fitness(cromossomo,papeis):
        global db
        DB.abrir()
        tPapeis = []
        tAgentes = []
        resultPapeis = []
        resultAfinit = []
        for pos,lista in enumerate(papeis):

            for i in lista:
                tPapeis.append([pos,cromossomo[i]])
                
        for i in cromossomo:
            for j in cromossomo:
                if i != j:
                    tAgentes.append([i,j])
        
        cursor = db.cursor()
        for i in tPapeis:
            cursor.execute("""
            SELECT producao, experiencia, valor FROM Papeis WHERE idpapel = ? AND idAgente = ?
            """,(i[0],i[1]))
            resultPapeis.append(cursor.fetchall())
        
        for i in tAgentes:
            cursor.execute("""
            SELECT afinidade FROM Afinidades WHERE idAgente = ? AND idOutro = ?
            """,(i[0], i[1]))
            resultAfinit.append(cursor.fetchall())
        DB.fechar()
        return resultPapeis,resultAfinit
        
        
    
    @staticmethod
    def Afinidade(agente,outro):
        global db
        DB.abrir()
        cursor = db.cursor()
        cursor.execute("""
        SELECT afinidade FROM Afinidades WHERE idAgente = ? AND idOutro = ?
        """,(agente, outro))
        valor = cursor.fetchall()
        return valor[0][0]
    
    @staticmethod       
    def Papeis(idagente,idpapel):
        global db
        DB.abrir()
        cursor = db.cursor()
        cursor.execute("""
        SELECT producao, experiencia, valor FROM Papeis WHERE idpapel = ? AND idAgente = ?
        """,(idpapel,idagente))
        valor = cursor.fetchall()
        return valor[0]
    
    @staticmethod
    def Preco(idagente,idpapel):
        return Get.Papeis(idagente, idpapel)[2]
    
    @staticmethod
    def Experiencia(idagente,idpapel):
        return Get.Papeis(idagente, idpapel)[1]
    
    @staticmethod
    def Producao(idagente,idpapel):
        return Get.Papeis(idagente, idpapel)[0]
    
    @staticmethod
    def IdsAgente():
        global db
        DB.abrir()
        cursor = db.cursor()
        cursor.execute("""
            SELECT id FROM Agentes;
        """)
        var = cursor.fetchall()
        return [i[0] for i in var]
    
    @staticmethod
    def Agentes():
        global db
        DB.abrir()
        cursor = db.cursor()
        cursor.execute("""
            SELECT * FROM Agentes;
        """)
        var = cursor.fetchall()
        return var
    
    @staticmethod
    def NomeAgentes(ida):
        global db
        DB.abrir()
        cursor = db.cursor()
        cursor.execute("""
            SELECT nome FROM Agentes WHERE id = ?;
        """,(ida,))
        var = cursor.fetchall()
        return var[0][0]
    
class Dados_rand:
    @staticmethod       
    def add():
        Dados_rand.add_agentes()
        print("nomes adicionados!!!!!!!")
        Dados_rand.add_papeis()
        print("papeis adicionados!!!!!!!")
        Dados_rand.add_Afinidades()
        print("afinidades adicionados!!!!!!!")
    
    @staticmethod    
    def add_Afinidades():#TODO: realizar commit
        import random
        adicionado =[]
        valores = ["Ruim","regular","Bom","Excelente"]
        '''
        de 0 a 10 ruim
        de 10 a 40 regular
        de 40 a 80 bom 
        de 80 a 100 excelente
        '''
        def GetAF(ent):
            if ent < 0.1:
                return 0
            elif ent < 0.4:
                return 1
            elif ent < 0.8:
                return 2
            else:
                return 3
        
        lista = DB.getIdsAgente()
        for i in lista:
            for j in lista:
                if i != j and j not in adicionado:
                    val = valores[GetAF(random.random())]
                    DB.add_Afinidade(i, j, val)
                    DB.add_Afinidade(j, i, val)
            adicionado.append(i)                
    
    @staticmethod
    def add_papeis():#TODO: papel gerente removido.
        import random 
        nomes_papeis = ["Analista de requisitos","Arquiteto(a) de software","Programador(a)","Testador(a)"]
        
        for j in DB.getIdsAgente():
            for i,nome in enumerate(nomes_papeis):
                prod = random.randint(0,100)
                exp = random.randint(0,100)
                val = random.randint(30,200)
                DB.add_Papel(j, i, nome, prod, exp, val)
    
    @staticmethod
    def add_agentes():
        file = open('nomes','r')
        names = [i[:-1] for i in file.readlines()] 
        for i,nome in enumerate(names):
            DB.add_Agente(i, nome)
    
    @staticmethod
    def add_Ferias():
        '''
        para cerca de 75% dos agentes:
        --colocar a data da seguinte forma:
            -->variacao de dia uniforme de 1 a 30
            -->mes gaussiana com cento no mes atual e desvio de 1 mes
            -->ano 75%: mesmo ano 25%:proximo ano
            ---> o fim eh sempre 1 mes do inicio.
        --> variar disponibilidade, horas por dia.
        '''
        pass#TODO: vai cupadi

if __name__ == '__main__':
    coisa = DB
    coisa.abrir()
    coisa.criar_tables()
    coisa.addRandDate()
    '''
    nomes_papeis = ["Analista de requisitos","Arquiteto(a) de software","Programador(a)","Testador(a)"]
    
    for i in range(0,10):
        for j in range(4):
            print(coisa.getNomeAgentes(i),": ",nomes_papeis[j],"-->", coisa.getPapeis(i, j))
    '''
    coisa.fechar()
   
    
    