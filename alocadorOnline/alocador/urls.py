from django.conf.urls import url

from . import views

app_name = "alocador"

urlpatterns = [
    url(r'^$', views.index, name="index"),
    
    
    url(r'^(?P<user_id>[0-9]+)/resultado',views.resultado, name="resultado"),
    url(r'^(?P<user_id>[0-9]+)/avaliar', views.avaliarEquipe, name='avaliar'),
    url(r'^(?P<user_id>[0-9]+)/comparar',views.comparar,name="comparar"),
    url(r'^(?P<user_id>[0-9]+)/escolher', views.escolherAlocacao,name="escolher"),
    url(r'^(?P<user_id>[0-9]+)/alocar',views.alocarComAG,name="alocar"),
    url(r'^(?P<user_id>[0-9]+)/avUsuario',views.avaliar,name="avUsuario"),
    url(r'^(?P<user_id>[0-9]+)/testar',views.paramTeste,name="testar"),
    url(r'^(?P<user_id>[0-9]+)/salvarDados',views.salvarResultado,name="salvarDados"),
    url(r'^(?P<user_id>[0-9]+)/logout', views.logout,name='logout'),
    url(r'^resultado', views.gerarResultados, name='verificarResultados'),
    
    #apis
    url(r'^(?P<user_id>[0-9]+)/userinfo',views.informacoesDoUsuario, name="userinfo"),
    url(r'^(?P<user_id>[0-9]+)/menu',views.menu, name="menu"),
    url(r'cadastro/$',views.cadastro,name="cadastro"),  
    url(r'login_init/$',views.loginP,name="login_init"),
    url(r'login/$',views.login,name="login"),

    ]