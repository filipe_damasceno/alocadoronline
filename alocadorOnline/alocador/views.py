# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.http import HttpResponse
import os
from .ag.genetcAlgoritm import AG,Cromossomo,DB
from .ag import gerarteste,comparador
from alocador.models import Usuario,Alocacoes,Resultados
from django.utils.timezone import datetime
import random as r
import timeit
import numpy as np
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt
# Create your views here.

logado = dict()
ID = dict()

def autenticado(user_id):
    global logado
    print(len(logado))
    print(user_id)
    return user_id in logado

def idLogado(user_id):
    global logado
    return int(user_id)

def gerarChave(id):
    global logado, ID
    a =sum(r.sample(range(2000),20))
    if id in ID:
       if ID[id] in logado:
           del logado[ID[id]]
       del ID[id]
       
    if a in logado:
        return gerarChave(id)
    logado[a]=id
    ID[id]=a
    return id

def index(request):
    return render(request, 'alocador/index.html')

def loginP(request):
    return render(request, 'alocador/login.html')

def paramTeste(request,user_id):
    if autenticado(user_id):
        return render(request, 'alocador/index.html')
    return render(request, 'testes/inicio.html',{'id':user_id})

'''OK'''
def escolherAlocacao(request,user_id):
    if autenticado(user_id):
        return render(request, 'alocador/index.html')
    user = Usuario.objects.get(id=idLogado(user_id))
    alocacoes = Alocacoes.objects.filter(usuario=user)
    
    cromossomos = [i.cromoObject() for i in alocacoes]
    
    return render(request, 'testes/comparar.html', {'individuos':cromossomos,'id':user_id,'nome':user.nome})

def resultado(request,user_id):
    if autenticado(user_id):
        return render(request, 'alocador/index.html')
    try:
        analistas = int(request.POST['analistas'])
        arquitetos = int(request.POST['arquitetos'])
        programadores = int(request.POST['programadores'])
        testadores = int(request.POST['testadores'])
        geracoes = int(request.POST['geracoes'])
    except:
        return render(request, 'alocador/alocarWithAG.html',{
            'error_message':"todos os campos devem ser preenchidos",
            'id':user_id})
    if sum([analistas,arquitetos,programadores,testadores]) < 2:
        return render(request, 'alocador/alocarWithAG.html',{
            'error_message':"deve-se escolher no minimo 2 individuos para uma equipe",
            'id':user_id})
    ag = AG(nAnalistas_req=analistas,\
            nArquitetos_soft=arquitetos,\
            nProgramadores=programadores,\
            nTestadores=testadores,\
            max_geracao=geracoes)
    ag.ag()
    dados = ag.central(ag.pop.getDadosEstatisticos())
    melhores = ag.pop.retornarMelhores()
    return render(request, 'alocador/resultado.html', {
        'individuos': dados[:20],
        'id': user_id})

def alocarComAG(request,user_id):
    if autenticado(user_id):
        return render(request, 'alocador/index.html')
    return render(request, 'alocador/alocarWithAG.html',{'id':user_id})

"OK"   
def avaliarEquipe(request,user_id):
    if autenticado(user_id):
        return render(request, 'alocador/index.html')
    ind = tratarDados(request.POST['choice'])
    return render(request, 'alocador/detalhes.html', {"agentes": ind,"id":user_id})

def comparar(request,user_id):
    if autenticado(user_id):
        return render(request, 'alocador/index.html')
    ind = request.POST['choice']
    al = Alocacoes.objects.get(id=ind)
    ids = al.getIds()
    #print("geracoes",(20*len(ids))/5)
    ag = AG(nAnalistas_req=al.analistas,\
            nArquitetos_soft=al.arquitetos,\
            nProgramadores=al.programadores,\
            nTestadores=al.testadores,\
            max_geracao=5,\
            ids=ids)
    #((20*len(ids))/5)+60,\
    aginicio = timeit.default_timer()
    ag.ag()
    dados = [ag.central(ag.pop.getDadosEstatisticos())[0]]
    agfim = timeit.default_timer()
    resultado_tempo = int(agfim - aginicio)
    h = resultado_tempo // 60**2
    hmod = resultado_tempo % 60**2
    min = hmod // 60
    minmod = hmod % 60
    segundo = minmod 
    
    tempo = str(h)+ ":"+str(min)+":"+str(segundo)
    
    dados += ag.pop.retornarMelhores()
    valores = [str(i.genes)[1:-1] for i in dados]
    r = Resultados(alocacoes=al,\
                   tempo = tempo,\
                   melhor_custo_beneficio=valores[0],\
                   melhor_custo=valores[1],\
                   melhor_afinidade=valores[2],\
                   melhor_produtividade=valores[3],\
                   melhor_experiencia=valores[4])
    r.save()
    '''
    ob = al.cromoObject().objetivos
    v=0
    for i in dados:
        radar([ob]+[i.objetivos],v)
        v+=1
    #'''
    caminho = ["graficos/"+str(user_id)+"-1.JPEG","graficos/"+str(user_id)+"-2.JPEG","graficos/"+str(user_id)+"-3.JPEG","graficos/"+str(user_id)+"-4.JPEG","graficos/"+str(user_id)+"-5.JPEG"]
    radar(al.cromoObject().objetivos,[i.objetivos for i in dados],user_id)
    result = comparador.avaliarMassa(dados,al.cromoObject(),caminho)
    media = sum([i.resultado for i in result])/5
    #result.sort(key = lambda r: r.resultado,reverse=True)
    
    return render(request, 'testes/resultado.html', {"dados":result,"id":user_id,"media":media})

"OK"
def cadastro(request):
    try:
        nome = request.POST['nome']
        email = request.POST['email']
        emp = request.POST['organizacao']
        sen1 = request.POST['senha']
        sen2 = request.POST['senha2']
    except:
        return render(request, 'alocador/index.html',{'error_message': "Cadastro incompleto!"})
    if sen1 != sen2:
        return render(request, 'alocador/index.html',{'error_message': "As senhas nao sao iguais!"})
    
    user = Usuario(nome = nome, email = email, organizacao = emp, senha=sen1)
    user.save()
    
    user_id = gerarChave(user.id)
    
    return render(request, 'alocador/init.html',{'id': user_id})

def menu(request,user_id):
    if autenticado(user_id):
        return render(request, 'alocador/index.html')
    return render(request, 'alocador/init.html',{'id': user_id})
    
"OK"
def login(request):
    global logado
    try:
        nome = request.POST['nome']
        senha = request.POST['senha']
    except:
        return render(request, 'alocador/login.html',{'error_message': "Cadastro incompleto!"})
    
    try:
        user = Usuario.objects.get(nome=nome, senha=senha)
    except:
        return render(request, 'alocador/login.html',{'error_message': "senha ou usuario incorretos"})
    
    user_id = gerarChave(user.id)
    
    return render(request, 'alocador/init.html',{'id': user_id})

    
def logout(request,user_id):
    if not autenticado(user_id):
        return render(request, 'alocador/index.html')
    global logado,ID
    id = logado[int(user_id)]
    logado[int(user_id)] = None
    ID[id] = None
    print(logado.items())
    del id
    return render(request, 'alocador/index.html')


'''OK'''
def avaliar(request,user_id):
    if autenticado(user_id):
        return render(request, 'alocador/index.html')
    user = idLogado(user_id)
    try:
        result = dict(request.POST)
        result = dict([(i,result[i][0]) for i in result])
        
    except:
        return render(request, 'testes/inicio.html',{'id':user_id})
    gerarteste.gerarPagina(int(result['Nids']))
    result['id']=user_id
    return render(request, 'testes/pagina.html',result)

"OK"
def salvarResultado(request,user_id):
    if autenticado(user_id):
        return render(request, 'alocador/index.html')
    
    user = Usuario.objects.get(id=idLogado(user_id))
    result = dict(request.POST)
    result = dict([(i,result[i][0]) for i in result])
    
    alocacao = result['alocacao']
    tamanho = int(result['Nids'])
    analistas = int(result['analistas'])
    arquitetos = int(result['arquitetos'])
    programadores = int(result['programadores'])
    testadores = int(result['testadores'])
    IDs = result['IDs']
                
    afinidade = int(result['afinidade'])
    produtividade = int(result['produtividade'])
    experiencia = int(result['experiencia'])
    custo = int(result['custo'])
    
    tempo = result['horas']+":"+result['minutos']+":"+result['segundos']
    if len(alocacao) > 1 and len(alocacao.split(",")) == sum([analistas,arquitetos,programadores,testadores]) :
        alocado = Alocacoes(usuario=user,\
                            cromossomo=alocacao,\
                            analistas=analistas,\
                            arquitetos=arquitetos,\
                            programadores=programadores,\
                            testadores=testadores,\
                            ids=IDs,\
                            tempo=tempo,\
                            Nafinidades=afinidade,\
                            Nprodutividade=produtividade,\
                            Nexperiencia=experiencia,\
                            Ncusto=custo)#TODO:tirar s
        alocado.save()
        lista = [int(i) for i in alocacao.split(",")]
        ids = [int(i) for i in IDs.split(",")]
        equipe = Cromossomo(tamanho,ids,[analistas,arquitetos,programadores,testadores],DB,lista)
        return render(request, 'alocador/detalhes.html',{"agentes": equipe,"error_message": "alocacao N: "+str(alocado.id),"id" : user_id})
    return render(request, 'alocador/init.html', {"error_message": "ERRO !!!","id": user_id})


#TODO: gerar graficos
#TODO: resultados la do models
def informacoesDoUsuario(request,user_id):
    if autenticado(user_id):
        return render(request, 'alocador/index.html')
    
    user = Usuario.objects.get(id=idLogado(user_id))
    alocacoes = Alocacoes.objects.filter(usuario=user)
    dados = dict()
    dados["tempo"]= [0,0,0]
    dados["afinidade"]=0
    dados["produtividade"]=0
    dados["experiencia"]=0
    dados["custo"]=0
    dados["qtd"]=len(alocacoes)
    for i in alocacoes:
        dados["tempo"][0] += i.tempo.hour
        dados["tempo"][1] += i.tempo.minute
        dados["tempo"][2] += i.tempo.second
        dados["afinidade"] += i.Nafinidades
        dados["produtividade"] += i.Nprodutividade
        dados["experiencia"] += i.Nexperiencia
        dados["custo"] += i.Ncusto
    print(dados)
    
    return render(request, 'alocador/user_info.html',{'user': user, 'dados':dados})

def tratarDados(entrada):
    
    dec,individuo = str(entrada)[2:-2].split('], [')
    individuo = [int(i) for i in individuo.split(',')]
    dec = [range(int(i[6:][0]),int(i[6:][3])) for i in dec.split('), ')]
    ids = DB.getIdsAgente()
    return Cromossomo(len(individuo),ids,None,DB,individuo,dec)



def radar(alocacao, fitness, userid):
    
    class Radar(object):
    
        def __init__(self, fig, titles, labels, rect=None):
            if rect is None:
                rect = [0.05, 0.05, 0.95, 0.95]
    
            self.n = len(titles)
            self.angles = np.arange(90, 90+360, 360.0/self.n)
            self.axes = [fig.add_axes(rect, projection="polar", label="axes%d" % i) 
                             for i in range(self.n)]
    
            self.ax = self.axes[0]
            self.ax.set_thetagrids(self.angles, labels=titles, fontsize=30)
    
            for ax in self.axes[1:]:
                ax.patch.set_visible(False)
                ax.grid(True)
                ax.xaxis.set_visible(False)
    
            for ax, angle, label in zip(self.axes, self.angles, labels):
                ax.set_rgrids(range(1, 11), angle=angle, labels=label)
                ax.spines["polar"].set_visible(True)#False)
                ax.set_ylim(0, 5)
    
        def plot(self, values, *args, **kw):
            angle = np.deg2rad(np.r_[self.angles, self.angles[0]])
            values = np.r_[values, values[0]]
            self.ax.plot(angle, values, *args, **kw)
    
    
    cores = ["r","b","g","y","c"]
    nomes = ["custo_beneficio","Custo","Afinidade","Produtividade","Experiencia"]
    cont=1
    for nome,cor,dados in zip(nomes,cores,fitness):

        fig = plt.figure(figsize=(9, 8))
        titles = ["E","C","P","A"]#"experiencia","custo","produtividade","afinidade"]#list("ABCDE")
        
        labels = [[""]*5]*4#[["20","40","60","80","100"] for i in range(4)]
        radar = Radar(fig, titles, labels,[0.1, 0.1, 0.80, 0.80])
        
        def normalize(v):
            return [(5*int(v[3]))/99,5*(1-((float(v[1])-30)/(200-30))),(5*int(v[2]))/99,(5*int(v[0]))/10]
        
    #     cores = ["k","r","b","g","y","c"]
    #     nomes = ["sua Alocacao","custo_beneficio","Custo","Afinidade","Produtividade","Experiencia"]
    #     radar = Radar(fig, titles, labels,[0.1, 0.1, 0.80, 0.80])
    #     for nome,cor,dados in zip(nomes,cores,fitness):
            #print(nome,cor,dados,normalize(dados))
        
        radar.plot(normalize(alocacao),  "-p", lw=2, color="k", alpha=1, label="sua Alocacao")
        radar.plot(normalize(dados),  "-p", lw=2, color=cor, alpha=1, label=nome)
        #radar.plot(normalize([10,200,99,99]),"-p", lw=2, color="b", alpha=1, label="second")
        #radar.plot([3, 4, 4, 2.3], "-p", lw=2, color="g", alpha=0.4, label="third")
        radar.ax.legend()
        BASE_DIR = os.path.split(os.path.dirname(os.path.abspath(__file__)))[0]
        filepath = os.path.join(BASE_DIR,"alocador\\static\\graficos\\"+str(userid)+"-"+str(cont)+".JPEG")
        cont+=1
        fig.savefig(filepath)
    del radar,dados,fig,cores,nomes,cont,BASE_DIR,filepath,titles,labels
    #enter image description here
    
def verificarResultados(request):
    
    usuarios = Usuario.objects.all()
    
    estatisitca = []
    
    for i in usuarios:
        estatisitca += Alocacoes.objects.filter(usuario=i)
    
    estatisitca.sort(key=lambda id: id.usuario.id, reverse=False)
    
    return render(request, 'alocador/estatisticas.html',{"est": Alocacoes.objects.all()})
    

'''

1- gerar o grafico de radar para todos os individuos, e todos os testes.
2- gerar a media para os tempos, comparando-os com a alocacao do participante.
3- gerar o grafico com as caracteristicas que sao clicadas para a alocacao.
4- gerar o grafico de semelhanca entre os resultados e o que foi comparado.

'''      
def gerarResultados(request):
    
    usuarios = Usuario.objects.all()
    medias = dict()
    for usuario in usuarios:
        alocacoes = Alocacoes.objects.filter(usuario=usuario)
        afinidade,produtividade,experiencia,custo = 0,0,0,0
        
        print(usuario.nome)
        
        for alocacao in alocacoes:
            resultados = Resultados.objects.filter(alocacoes=alocacao)
            tempo = [0,0,0]
            objetivos = np.array([[0.0,0.0,0.0,0.0]]*5)

            afinidade= alocacao.Nafinidades
            produtividade = alocacao.Nprodutividade
            experiencia = alocacao.Nexperiencia
            custo = alocacao.Ncusto
            
            semelhancas = np.array([0,0,0,0,0])
            print("alocacao com:",len(alocacao.getIds()),"agentes")
            for resultado in resultados:
                #print(resultado.getAll[0])
                #tempo[0],tempo[1],tempo[2] = resultado.tempo.hour,resultado.tempo.minute,resultado.tempo.second
                
                tempo1 = ((60**2)*resultado.tempo.hour)+(60*resultado.tempo.minute)+resultado.tempo.second
                #print(tempo1)
                objetivos += np.array([i.objetivos for i in resultado.getAll])
                #print(objetivos)
                semelhancas += np.array(resultado.semelhancas())
                #print(semelhancas)
            
            objetivos /= len(resultados)
            semelhancas //= len(resultados)
            
            tempo1 = tempo1//len(resultados)
            tempo[0] = tempo1//(60**2)
            tempo1m = tempo1 % (60**2)
            tempo[1] = tempo1m // 60
            tempo1s = tempo1m % 60
            tempo[2] = tempo1s
            #for i in range(3):
            #    tempo[i] /= (len(resultados) if len(resultados) > 0 else 1) 
            
            print('numero de testes realizados:',len(resultados))
            print('tempo em segundos: \ntempo do ag:',tempo1,'\ntempo do usuario: ',((60**2)*alocacao.tempo.hour)+(60*alocacao.tempo.minute)+alocacao.tempo.second)
            print('tempo de alocação: \ntempo do ag:','\ntempo do usuario: ',alocacao.tempo,tempo)
            print("alocação do usuario:\n",alocacao.cromoObject().objetivos,"\nmedia de alocação do algoritmo:\n",objetivos)
            radar(alocacao.cromoObject().objetivos,objetivos,str(usuario.nome).replace(" ", "-")+"-"+str(len(alocacao.getIds())))
            print('Semelhancas:', semelhancas)
            print('caracteristicas:',afinidade,produtividade,experiencia,custo)
            print("\n"*2,"#$"*50,"\n"*2)
        
    
    
    
