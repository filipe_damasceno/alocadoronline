from django.db import models
from .ag.genetcAlgoritm import Cromossomo,DB
from .ag.comparador import avaliarDados

# Create your models here.

class Usuario(models.Model):
    nome = models.CharField(max_length=200)
    email = models.CharField(max_length=200)
    organizacao = models.CharField(max_length=200)
    senha = models.CharField(max_length=200)

class Alocacoes(models.Model):
    usuario = models.ForeignKey(Usuario,on_delete=models.CASCADE)
    cromossomo = models.CharField(max_length=200)
    analistas = models.IntegerField()
    arquitetos = models.IntegerField()
    programadores = models.IntegerField()
    testadores = models.IntegerField()
    ids = models.CharField(max_length=200)
    tempo = models.TimeField()
    Nafinidades = models.IntegerField()
    Nprodutividade = models.IntegerField()
    Nexperiencia = models.IntegerField()
    Ncusto = models.IntegerField()
    
    def getIds(self):
        return [int(i) for i in self.ids.split(",")]
    
    def cromoObject(self):
        CROMO = [int(i) for i in self.cromossomo.split(",")]
        IDS = [int(i) for i in self.ids.split(",")]
        equipe = Cromossomo(len(CROMO),IDS,[self.analistas,self.arquitetos,self.programadores,self.testadores],DB,CROMO)
        equipe.equipe = self.id
        return equipe
    

class Resultados(models.Model):
    alocacoes = models.ForeignKey(Alocacoes,on_delete=models.CASCADE)
    melhor_custo_beneficio = models.CharField(max_length=200)
    melhor_custo = models.CharField(max_length=200)
    melhor_afinidade = models.CharField(max_length=200)
    melhor_produtividade = models.CharField(max_length=200)
    melhor_experiencia = models.CharField(max_length=200)
    tempo = models.TimeField()
    
    def semelhancas(self):
        return avaliarDados(self.getAll,self.alocacoes.cromoObject())
    
    def cromoCustoBeneficio(self):
        CROMO = [int(i) for i in self.melhor_custo_beneficio.split(",")]
        equipe = Cromossomo(tamanho=len(CROMO),codificacao=[self.alocacoes.analistas,self.alocacoes.arquitetos,self.alocacoes.programadores,self.alocacoes.testadores],DB=DB,genes=CROMO)
        equipe.equipe = self.id
        return equipe
    
    def cromoCusto(self):
        CROMO = [int(i) for i in self.melhor_custo.split(",")]
        equipe = Cromossomo(tamanho=len(CROMO),codificacao=[self.alocacoes.analistas,self.alocacoes.arquitetos,self.alocacoes.programadores,self.alocacoes.testadores],DB=DB,genes=CROMO)
        equipe.equipe = self.id
        return equipe
    
    def cromoAfinidade(self):
        CROMO = [int(i) for i in self.melhor_afinidade.split(",")]
        equipe = Cromossomo(tamanho=len(CROMO),codificacao=[self.alocacoes.analistas,self.alocacoes.arquitetos,self.alocacoes.programadores,self.alocacoes.testadores],DB=DB,genes=CROMO)
        equipe.equipe = self.id
        return equipe
    
    def cromoProdutividade(self):
        CROMO = [int(i) for i in self.melhor_produtividade.split(",")]
        equipe = Cromossomo(tamanho=len(CROMO),codificacao=[self.alocacoes.analistas,self.alocacoes.arquitetos,self.alocacoes.programadores,self.alocacoes.testadores],DB=DB,genes=CROMO)
        equipe.equipe = self.id
        return equipe
    
    def cromoExperiencia(self):
        CROMO = [int(i) for i in self.melhor_experiencia.split(",")]
        equipe = Cromossomo(tamanho=len(CROMO),codificacao=[self.alocacoes.analistas,self.alocacoes.arquitetos,self.alocacoes.programadores,self.alocacoes.testadores],DB=DB,genes=CROMO)
        equipe.equipe = self.id
        return equipe
    @property
    def getAll(self):
        return [self.cromoCustoBeneficio(),self.cromoCusto(),self.cromoAfinidade(),self.cromoProdutividade(),self.cromoExperiencia()]

